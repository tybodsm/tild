---
layout: page
title: About
permalink: /about
---

<p align="center">
  <img width="30%" src="/assets/image/back.png">
</p>

I'm Tyler: currently a Quantitative UX researcher at Google. This mini-blog is an effort to stay busy as I hit the halfway mark of my ~4 month work-leave due to severe sciatic issues. Hence the name of the blog -- I'm quite literally taking these weeks lying down.

Most posts will focus on various data-science subjects, but some may be personal projects or totally random. The goal is to have a new post every week for the next 9 weeks. Some will be longer and more involved than others. It just depends on what I can do in a given week. 

But hopefully some fun posts and some solid sanity maintenence come out of this! 


